ARG CI_REGISTRY
FROM debian:bookworm-backports

RUN <<EOF
apt-get update
apt-get -y upgrade
apt-get -y install curl gnupg2
curl -sL https://deb.nodesource.com/setup_20.x | bash -
apt-get -y install nodejs
EOF
