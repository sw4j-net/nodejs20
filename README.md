# Introduction

This project creates a docker image with [node.js](https://nodejs.org/) 20 installed.

The image can be used to build software using node.js.

This original repository is at https://git.sw4j.net/sw4j-net/nodejs20

This repository is mirrored to https://gitlab.com/sw4j-net/nodejs20
